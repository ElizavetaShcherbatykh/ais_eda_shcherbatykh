import React from 'react';
import './App.scss';
import { Router } from 'react-router-dom';
import history from './utils/history';

import Layout from './components/Layout';

function App() {
  return (
    <>
        <Router history={history}>
          <Layout />
        </Router>
    </>
  );
}

export default App;