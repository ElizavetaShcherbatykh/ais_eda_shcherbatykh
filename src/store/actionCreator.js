export const actionCreator = action => {
    function createAction (action) {
        return Object.assign((payload) => ({ type: action, payload }), { type: action });
    }

    return {
        request: createAction(`${action}_REQUEST`),
        success: createAction(`${action}_SUCCESS`),
        failed: createAction(`${action}_FAILED`)
    }
};
