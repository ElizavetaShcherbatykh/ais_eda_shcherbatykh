import { combineReducers } from 'redux';
import userReducer from './../modules/user/userReducer';


export default combineReducers({
	user: userReducer,
});