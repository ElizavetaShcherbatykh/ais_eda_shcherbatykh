import React from 'react';
import {HeaderBody} from './styles'

const Header = () => (
    <HeaderBody>
        <h1>Header</h1>
    </HeaderBody>
);

export default Header;