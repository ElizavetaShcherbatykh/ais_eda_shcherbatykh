import React from 'react';
import { Switch, Route } from "react-router-dom";
import { LayoutBody } from './styles';

import Header from './../Header';
import HomePage from 'pages/HomePage';
import WelcomePage from 'pages/WelcomePage';
import ErrorPage from 'pages/ErrorPage';

const Layout = () => {
	return (
		<LayoutBody>
		<Header />
		<main>
			<Switch>
				<Route path="/404">
					<ErrorPage />
				</Route>
				<Route path="/user/:login">
					<HomePage/>
				</Route>
				<Route exact path="/">
					<WelcomePage />
				</Route>
				<Route path="*">
					<ErrorPage />
				</Route>
			</Switch>
		</main>
	</LayoutBody>
	)
	
};

export default Layout;