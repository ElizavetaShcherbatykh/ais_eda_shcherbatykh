import React from 'react';
import {ErrorPageBody} from './styles';

const ErrorPage = () => (
    <ErrorPageBody>
        <h1>Error Page</h1>
    </ErrorPageBody>
);

export default ErrorPage;