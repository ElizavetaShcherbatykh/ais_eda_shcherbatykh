import React from 'react';
import { WelcomePageBody } from './styles';

const WelcomePage = () => (
    <WelcomePageBody>
      <h1>Welcome Page</h1>  
    </WelcomePageBody>
);

export default WelcomePage;