import React from 'react';
import { HomePageBody } from './styles';

const HomePage = () => (
    <HomePageBody>
        <h1>Home Page</h1>
    </HomePageBody>
);

export default HomePage;